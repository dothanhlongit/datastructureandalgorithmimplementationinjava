package data.structure.algorithm.java.queue;

public class Queue<T> {
    private Node<T> first;
    private Node<T> last;
    private int length;

    Queue() {
        this.first = null;
        this.last = null;
        this.length = 0;
    }

    public Node<T> enQueue(T data) {
        return enQueue(new Node<T>(data));
    }

    public Node<T> enQueue(Node<T> node) {
        if (node == null) {
            throw new IllegalArgumentException();
        }

        if (this.length == 0) {
            this.first = node;
            this.last = node;
        } else {
            node.setNext(this.last);
            this.last = node;
        }

        this.length++;
        return this.last;
    }

    public Node<T> deQueue() {
        if (this.length == 0) {
            return null;
        }
        Node<T> node = this.first;

        if (this.length == 1) {
            this.first = null;
            this.last = null;
            return node;
        }

        Node<T> preNode = traversalToIndex(this.length - 2);
        preNode.setNext(null);

        this.length--;
        return node;
    }

    public Node<T> traversalToIndex(int index) {
        if (this.length == 0) {
            return null;
        }
        if (index < 0 || index >= this.length) {
            throw new IllegalArgumentException();
        }

        Node<T> node = this.last;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }

        return node;
    }

    public Node<T> peek() {
        return this.first;
    }

    public void printContent() {
        Node<T> node = this.last;
        System.out.println("last");
        StringBuilder stringBuilder = new StringBuilder();
        while (node != null) {
            stringBuilder.append(node.getData()).append(" -----> ");
            node = node.getNext();
        }
        System.out.println(stringBuilder);
        System.out.println("first");
    }
}
