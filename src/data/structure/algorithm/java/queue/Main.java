package data.structure.algorithm.java.queue;

public class Main {

    public static void main(String[] args) {
        Queue<String> queue = new Queue<>();
        queue.enQueue("1");
        queue.enQueue("2");
        queue.enQueue("3");
        queue.printContent();
        queue.deQueue();
        queue.printContent();
        queue.deQueue();
        queue.printContent();
        queue.deQueue();
        queue.printContent();
    }
}
