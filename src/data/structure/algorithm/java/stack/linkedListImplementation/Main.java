package data.structure.algorithm.java.stack.linkedListImplementation;

public class Main {

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.pop();
        stack.printContent();
    }
}
