package data.structure.algorithm.java.stack.linkedListImplementation;

public class Stack<T> {
    private Node<T> top;
    private Node<T> bottom;
    private int length;

    Stack() {
        this.top = null;
        this.bottom = null;
        this.length = 0;
    }

    public Node<T> push(T data) {
        return push(new Node<T>(data));
    }

    public Node<T> push(Node<T> node) {
        if (node == null) {
            throw new IllegalArgumentException();
        }

        if (this.length == 0) {
            this.top = node;
            this.bottom = node;
        } else {
            node.setNext(this.top);
            this.top = node;
        }

        this.length++;
        return this.top;
    }

    public Node<T> pop() {
        if (this.length == 0) {
            return null;
        }
        Node<T> node = this.top;

        if (this.length == 1) {
            this.top = null;
            this.bottom = null;
            return node;
        }

        this.top = this.top.getNext();
        this.length--;
        return node;
    }

    public Node<T> peek() {
        return this.top;
    }

    public void printContent() {
        Node<T> node = this.top;
        System.out.println("top");
        while (node != null) {
            System.out.println(node.getData() + " -----> ");
            node = node.getNext();
        }
        System.out.println("bottom");
    }
}
