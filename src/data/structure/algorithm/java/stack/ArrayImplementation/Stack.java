package data.structure.algorithm.java.stack.ArrayImplementation;

import java.util.ArrayList;
import java.util.List;

public class Stack<T> {
    private List<T> data = new ArrayList<>();

    public T push(T data) {
        if (data == null || data.equals("")) {
            throw new IllegalArgumentException();
        }
        this.data.add(data);

        return data;
    }

    public T pop() {
        if (this.data.size() == 0) {
            return null;
        }

        return this.data.remove(this.data.size() - 1);
    }

    public T peek() {
        return this.data.get(this.data.size() - 1);
    }

    public void printContent() {
        System.out.println("top");

        int index = this.data.size() -1;
        while (index >= 0) {
            System.out.println(this.data.get(index));
            index--;
        }

        System.out.println("bottom");
    }
}
