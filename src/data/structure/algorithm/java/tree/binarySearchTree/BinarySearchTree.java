package data.structure.algorithm.java.tree.binarySearchTree;

import java.util.ArrayDeque;

public class BinarySearchTree<T> {
    private final String prefix = "++++";
    private Node<T> root;
    private final CompareTool<T> compareTool;

    BinarySearchTree(CompareTool<T> compareTool) {
        this.root = null;
        this.compareTool = compareTool;
    }

    public void insert(T data) {
        if (data == null) {
            throw new IllegalArgumentException();
        }
        insert(new Node<T>(data));
    }

    public void insert(Node<T> node) {
        if (node == null) {
            throw new IllegalArgumentException();
        }
        if (this.root == null) {
            this.root = node;
            return;
        }

        Node<T> currentVisitedNode = this.root;
        while (true) {
            if (compareTool.isSmaller(node, currentVisitedNode)) {
                if (currentVisitedNode.getLeft() == null) {
                    currentVisitedNode.setLeft(node);
                    return;
                }
                currentVisitedNode = currentVisitedNode.getLeft();
            } else {
                if (currentVisitedNode.getRight() == null) {
                    currentVisitedNode.setRight(node);
                    return;
                }
                currentVisitedNode = currentVisitedNode.getRight();
            }
        }
    }

    public Node<T> lookup(T data) {
        if (data == null) {
            throw new IllegalArgumentException();
        }
        if (this.root == null) {
            return null;
        }

        Node<T> currentVisitedNode = this.root;
        Node<T> lookupNode = new Node<>(data);
        while (true) {
            if (compareTool.isEqual(currentVisitedNode, lookupNode)) {
                return currentVisitedNode;
            }
            if (compareTool.isSmaller(lookupNode, currentVisitedNode)) {
                if (currentVisitedNode.getLeft() == null) {
                    return null;
                }
                currentVisitedNode = currentVisitedNode.getLeft();
            } else {
                if (currentVisitedNode.getRight() == null) {
                    return null;
                }
                currentVisitedNode = currentVisitedNode.getRight();
            }
        }
    }

    public void printContent(Node<T> currentVisitedNode, String builder) {
        if (currentVisitedNode == null) {
            throw new IllegalArgumentException();
        }
        if (currentVisitedNode.getRight() != null) {
            printContent(currentVisitedNode.getRight(), builder + prefix);
        }

        System.out.println(builder + currentVisitedNode.getData());

        if (currentVisitedNode.getLeft() != null) {
            printContent(currentVisitedNode.getLeft(), builder + prefix);
        }
    }

    public Node<T> getRoot() {
        return this.root;
    }

    public void breadFirstSearchUsingLoop(StringBuilder sb) {
        if (this.root == null || sb == null) {
            return;
        }
        ArrayDeque<Node<T>> queue = new ArrayDeque<>();
        queue.addLast(this.root);

        while (queue.size() > 0) {
            Node<T> node = queue.removeFirst();
            sb.append(node.getData()).append(";");

            if (node.getLeft() != null) {
                queue.addLast(node.getLeft());
            }
            if (node.getRight() != null) {
                queue.addLast(node.getRight());
            }
        }
    }

    public void depthFirstSearchUsingRecursion(Node<T> node, StringBuilder sb) {
        if (node == null) {
            return;
        }

        sb.append(node.getData()).append(";");
        depthFirstSearchUsingRecursion(node.getLeft(), sb);
        depthFirstSearchUsingRecursion(node.getRight(), sb);
    }
}
