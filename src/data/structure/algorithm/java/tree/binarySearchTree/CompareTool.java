package data.structure.algorithm.java.tree.binarySearchTree;

public interface CompareTool<T> {
    boolean isSmaller(Node<T> compareNode, Node<T> comparedNode);

    default boolean isGreaterOrEqual(Node<T> compareNode, Node<T> comparedNode) {
        return !isSmaller(compareNode, comparedNode);
    }

    boolean isEqual(Node<T> compareNode, Node<T> comparedNode);
}
