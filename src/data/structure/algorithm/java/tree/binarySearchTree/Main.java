package data.structure.algorithm.java.tree.binarySearchTree;

public class Main {

    public static void main(String[] args) {
        CompareTool<Integer> compareTool = new CompareTool<Integer>() {
            @Override
            public boolean isSmaller(Node<Integer> compareNode, Node<Integer> comparedNode) {
                if (compareNode == null || compareNode.getData() == null ||
                        comparedNode == null || comparedNode.getData() == null) {
                    throw new IllegalArgumentException();
                }
                return compareNode.getData() < comparedNode.getData();
            }

            @Override
            public boolean isEqual(Node<Integer> compareNode, Node<Integer> comparedNode) {
                return compareNode.getData().equals(comparedNode.getData());
            }
        };

        BinarySearchTree<Integer> binarySearchTree = new BinarySearchTree<>(compareTool);
        binarySearchTree.insert(1);
        binarySearchTree.insert(2);
        binarySearchTree.insert(-1);
        binarySearchTree.insert(-3);
        binarySearchTree.insert(3);
        binarySearchTree.printContent(binarySearchTree.getRoot(), "");
        int lookupData = 5;
        System.out.println("lookup "+lookupData+":" + binarySearchTree.lookup(lookupData));
        StringBuilder sb1 = new StringBuilder();
        binarySearchTree.breadFirstSearchUsingLoop(sb1);
        System.out.println("breadth first search using loop: " + sb1);

        StringBuilder sb2 = new StringBuilder();
        binarySearchTree.depthFirstSearchUsingRecursion(binarySearchTree.getRoot(), sb2);
        System.out.println("depth first search using recursion: " + sb2);
    }
}
