package data.structure.algorithm.java.linkedList.singly;


public class SinglyLinkedList<T> {
    private SingleNode<T> head;
    private SingleNode<T> tail;
    private int length;

    public SinglyLinkedList() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    public SingleNode<T> append(T data) {
        if (data == null) {
            throw new IllegalArgumentException();
        }

        return append(new SingleNode<>(data));
    }

    public SingleNode<T> append(SingleNode<T> node) {
        if (node == null) {
            throw new IllegalArgumentException();
        }

        if (this.length == 0) {
            this.head = node;
            this.tail = node;
        } else {
            this.tail.setNext(node);
            this.tail = node;
        }

        this.length++;
        return this.tail;
    }

    public SingleNode<T> prepend(T data) {
        if (data == null) {
            throw new IllegalArgumentException();
        }

        return prepend(new SingleNode<>(data));
    }

    public SingleNode<T> prepend(SingleNode<T> node) {
        if (node == null) {
            throw new IllegalArgumentException();
        }

        if (this.length == 0) {
            this.head = node;
            this.tail = node;
        } else {
            node.setNext(this.head);
            this.head = node;
        }

        this.length++;
        return this.head;
    }

    public SingleNode<T> insert(int index, T data) {
        if (data == null) {
            throw new IllegalArgumentException();
        }

        return insert(index, new SingleNode<>(data));
    }

    public SingleNode<T> insert(int index, SingleNode<T> node) {
        if (node == null || index < 0 || index >= this.length) {
            throw new IllegalArgumentException();
        }

        if (index == 0) {
            return prepend(node);
        } else {
            SingleNode<T> preNode = traversalToIndex(index - 1);
            node.setNext(preNode.getNext());
            preNode.setNext(node);
        }

        this.length++;
        return node;
    }

    public SingleNode<T> remove(int index) {
        if (this.length == 0) {
            return null;
        } else if (index < 0 || index >= this.length) {
            throw new IllegalArgumentException();
        }

        SingleNode<T> removedNode = traversalToIndex(index);
        if (index == 0) {
            return removeFirst();
        } else if (index == this.length - 1) {
            return removeLast();
        } else {
            SingleNode<T> preNode = traversalToIndex(index - 1);
            preNode.setNext(removedNode.getNext());
        }

        this.length--;
        return removedNode;
    }

    public SingleNode<T> removeFirst() {
        SingleNode<T> removedNode = this.head;
        if (length == 0) {
            return null;
        } else if (length == 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.head = this.head.getNext();
        }

        this.length--;
        return removedNode;
    }

    public SingleNode<T> removeLast() {
        SingleNode<T> removedNode = this.tail;

        if (this.length == 0) {
            return null;
        } else if (this.length == 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.tail = traversalToIndex(this.length - 2);
            this.tail.setNext(null);
        }

        this.length--;
        return removedNode;
    }

    public SingleNode<T> traversalToIndex(int index) {
        if (this.length == 0) {
            return null;
        } else if (index < 0 || index >= this.length) {
            throw new IllegalArgumentException();
        }

        SingleNode<T> node = this.head;
        int i = 1;
        while (i <= index) {
            node = node.getNext();
            i++;
        }
        return node;
    }

//    reverse() {
//
//    }

    public void printContent() {
        SingleNode<T> currentNode = this.head;
        StringBuilder stringBuilder = new StringBuilder();

        while (currentNode != null) {
            stringBuilder.append(currentNode.getData()).append("---");
            currentNode = currentNode.getNext();
        }

        System.out.println(stringBuilder);
    }
}

