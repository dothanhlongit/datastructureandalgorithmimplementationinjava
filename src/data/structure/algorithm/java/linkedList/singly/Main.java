package data.structure.algorithm.java.linkedList.singly;

public class Main {

    public static void main(String[] args) {
        SinglyLinkedList<Integer> singlyLinkedList = new SinglyLinkedList();
        singlyLinkedList.append(1);
        singlyLinkedList.append(2);
        singlyLinkedList.prepend(3);
        singlyLinkedList.remove(0);
        singlyLinkedList.printContent();
        singlyLinkedList.prepend(3);
        singlyLinkedList.remove(2);
        singlyLinkedList.printContent();
        singlyLinkedList.insert(1,2);
        singlyLinkedList.printContent();
        singlyLinkedList.insert(0,4);
        singlyLinkedList.insert(1,9);
        singlyLinkedList.printContent();
    }
}
