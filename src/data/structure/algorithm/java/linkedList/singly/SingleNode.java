package data.structure.algorithm.java.linkedList.singly;

public class SingleNode<T> {
    private T data;
    private SingleNode<T> next;

    SingleNode(T data, SingleNode<T> next) {
        this.data = data;
        this.next = next;
    }

    SingleNode(T data) {
        this.data = data;
    }

    public SingleNode<T> getNext() {
        return next;
    }

    public void setNext(SingleNode<T> next) {
        this.next = next;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
