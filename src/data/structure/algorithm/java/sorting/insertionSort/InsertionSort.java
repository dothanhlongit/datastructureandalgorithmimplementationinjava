package data.structure.algorithm.java.sorting.insertionSort;

public class InsertionSort {
    /**
     * bubble sort
     * O(n) in best case
     * O(n^2) in average case
     * O(n^2) in worst case
     */
    public static void insertionSort(int[] numArr) {
        if (numArr == null || numArr.length <= 1) {
            return;
        }

        for (int i = 1; i < numArr.length; i++) {
            boolean shift2Right = false;
            int preValue = numArr[0];
            for (int j = 0; j <= i; j++) {
                int temp = numArr[j];
                if (shift2Right) {
                    numArr[j] = preValue;
                } else if (numArr[j] > numArr[i]) {
                    numArr[j] = numArr[i];
                    shift2Right = true;
                }
                preValue = temp;
            }
        }
    }

    public static void printContentOfList(int[] numArr) {
        if (numArr == null || numArr.length == 0) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (int element : numArr) {
            sb.append(element).append("-->");
        }
        System.out.println(sb);
    }

    public static void main(String[] args) {
        int[] numArr = {8, 9, 2, 4, 6, 10};
        printContentOfList(numArr);
        insertionSort(numArr);
        printContentOfList(numArr);
    }
}
