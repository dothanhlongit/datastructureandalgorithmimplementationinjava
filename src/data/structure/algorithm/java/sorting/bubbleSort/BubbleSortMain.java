package data.structure.algorithm.java.sorting.bubbleSort;

public class BubbleSortMain {
    /**
     * bubble sort
     * O(n) in best case
     * O(n^2) in average case
     * O(n^2) in worst case
     */
    public static void bubbleSort(int[] numArr) {
        if (numArr == null || numArr.length <= 1) {
            return;
        }

        for (int i = numArr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (numArr[j] > numArr[j + 1]) {
                    int temp = numArr[j];
                    numArr[j] = numArr[j + 1];
                    numArr[j + 1] = temp;
                }
            }
        }
    }

    public static void printContentOfList(int[] numArr) {
        if (numArr == null || numArr.length == 0) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (int element : numArr) {
            sb.append(element).append("-->");
        }
        System.out.println(sb);
    }

    public static void main(String[] args) {
        int[] numArr = {8, 9, 2, 4, 6, 10};
        printContentOfList(numArr);
        bubbleSort(numArr);
        printContentOfList(numArr);
    }
}
