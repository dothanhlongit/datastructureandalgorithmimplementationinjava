package data.structure.algorithm.java.sorting.selectionSort;

public class SelectionSortMain {

    /**
     * selection sort
     * O(n^2) in best case
     * O(n^2) in average case
     * O(n^2) in worst case
     */
    public static void selectionSort(int[] numArr) {
        if (numArr == null || numArr.length <= 1) {
            return;
        }

        for (int i = 0; i < numArr.length; i++) {
            int smallestIndex = i;
            for (int j = i; j < numArr.length - 1; j++) {
                if (numArr[smallestIndex] > numArr[j]) {
                    smallestIndex = j;
                }
            }
            swap(numArr, i, smallestIndex);
        }
    }

    public static void swap(int[] numArr, int i, int j) {
        if (i < 0 || j < 0 || i >= numArr.length || j >= numArr.length) {
            throw new IllegalArgumentException();
        }
        int temp = numArr[i];
        numArr[i] = numArr[j];
        numArr[j] = temp;
    }

    public static void printContentOfList(int[] numArr) {
        if (numArr == null || numArr.length == 0) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        for (int element : numArr) {
            sb.append(element).append("-->");
        }
        System.out.println(sb);
    }

    public static void main(String[] args) {
        int[] numArr = {8, 9, 2, 4, 6, 10};
        printContentOfList(numArr);
        selectionSort(numArr);
        printContentOfList(numArr);
    }
}
