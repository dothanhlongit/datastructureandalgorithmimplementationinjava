package data.structure.algorithm.java.graph.undirectedAndUnweighted;

public class Main {

    public static void main(String[] args) {
        Graph<Integer> integerGraph = new Graph<>();
        integerGraph.addVertex(0);
        integerGraph.addVertex(1);
        integerGraph.addVertex(2);
        integerGraph.addVertex(3);
        integerGraph.addVertex(4);
        integerGraph.addVertex(5);
        integerGraph.addVertex(6);

        integerGraph.addEdge(0, 1);
        integerGraph.addEdge(0, 2);
        integerGraph.addEdge(1, 2);
        integerGraph.addEdge(1, 3);
        integerGraph.printContent();
    }
}
