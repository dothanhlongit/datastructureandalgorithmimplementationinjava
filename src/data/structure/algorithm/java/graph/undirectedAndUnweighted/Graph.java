package data.structure.algorithm.java.graph.undirectedAndUnweighted;

import java.util.LinkedHashMap;
import java.util.LinkedList;

public class Graph<T> {
    private LinkedHashMap<T, LinkedList<T>> adjacentList = new LinkedHashMap<>();
    private int numberOfNodes = 0;

    Graph() {

    }

    public void addVertex(T data) {
        if (data == null) {
            throw new IllegalArgumentException();
        }

        if (adjacentList.get(data) != null) {
            return;
        }

        adjacentList.put(data, new LinkedList<>());
        this.numberOfNodes++;
    }

    public void addEdge(T node1, T node2) {
        if (node1 == null || node2 == null) {
            throw new IllegalArgumentException();
        }

        LinkedList<T> adjacentNodes = adjacentList.get(node1);

        boolean node1IsNotExisted = adjacentNodes == null;
        boolean node1HasConnectedWithNode2 = adjacentNodes.contains(node2);
        boolean node2IsNotExisted = adjacentList.get(node2) == null;
        if (node1IsNotExisted || node1HasConnectedWithNode2 || node2IsNotExisted) {
            return;
        }
        adjacentNodes.add(node2);
    }

    public void printContent() {
        for (T node : this.adjacentList.keySet()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(node).append(":  ");
            for (T adjacentedNode : this.adjacentList.get(node)) {
                stringBuilder.append(adjacentedNode).append(",");
            }
            System.out.println(stringBuilder);
        }
    }
}
