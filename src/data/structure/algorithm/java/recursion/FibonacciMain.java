package data.structure.algorithm.java.recursion;

public class FibonacciMain {


    public static int recursiveFibonacci(int index) {
        if (index < 0) {
            throw new IllegalArgumentException();
        }
        if (index <= 1) {
            return index;
        }

        return recursiveFibonacci(index - 1) + recursiveFibonacci(index - 2);
    }

    public static int loopFibonacci(int index) {
        if (index < 0) {
            throw new IllegalArgumentException();
        }
        if (index == 0 || index == 1) {
            return index;
        } else if (index == 2) {
            return 1;
        } else if (index == 3) {
            return 2;
        }

        int pre1 = 1;
        int pre2 = 2;
        int result = 0;
        for (int i = 4; i <= index; i++) {
            result = pre1 + pre2;
            pre1 = pre2;
            pre2 = result;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println("value of index: 8:  " + recursiveFibonacci(8));
        System.out.println("value of index: 8:  " + loopFibonacci(8));
    }
}
