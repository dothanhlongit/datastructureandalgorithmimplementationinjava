package data.structure.algorithm.java.recursion;

public class FactorialMain {

    /**
     * this function don't support calculate the factorial of negative real number
     */
    public static int recursiveFactorial(int number) {
        if (number < 0) {
            throw new IllegalArgumentException();
        }
        if (number == 1) {
            return 1;
        }

        return number * recursiveFactorial(number - 1);
    }

    /**
     * this function don't support calculate the factorial of negative real number
     */
    public static int loopFactorial(int number) {
        if (number < 0) {
            throw new IllegalArgumentException();
        }

        int result = number;
        while (number >= 2) {
            number--;
            result = result * number;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("factorial of 10 using recursive func: " + FactorialMain.recursiveFactorial(10));
        System.out.println("factorial of 10 using loop func: " + FactorialMain.loopFactorial(10));
    }


}
