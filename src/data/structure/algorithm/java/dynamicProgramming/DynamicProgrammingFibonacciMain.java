package data.structure.algorithm.java.dynamicProgramming;

import java.util.Hashtable;

public class DynamicProgrammingFibonacciMain {
    private static int recursiveCounter = 0;
    private static int dynamicProgrammingCounter = 0;
    private static int loopCounter = 0;
    private static Hashtable<Integer, Integer> hashtable = new Hashtable<Integer, Integer>();

    public static int recursiveFibonacci(int index) {
        DynamicProgrammingFibonacciMain.recursiveCounter++;
        if (index < 0) {
            throw new IllegalArgumentException();
        }
        if (index <= 1) {
            return index;
        }

        return recursiveFibonacci(index - 1) + recursiveFibonacci(index - 2);
    }

    public static int loopFibonacci(int index) {
        if (index < 0) {
            throw new IllegalArgumentException();
        }
        if (index == 0 || index == 1) {
            return index;
        } else if (index == 2) {
            return 1;
        } else if (index == 3) {
            return 2;
        }

        int pre1 = 1;
        int pre2 = 2;
        int result = 0;
        for (int i = 4; i <= index; i++) {
            DynamicProgrammingFibonacciMain.loopCounter++;
            result = pre1 + pre2;
            pre1 = pre2;
            pre2 = result;
        }
        return result;
    }

    public static int dynamicFibonacci(int index) {
        DynamicProgrammingFibonacciMain.dynamicProgrammingCounter++;
        if (index < 0) {
            throw new IllegalArgumentException();
        }
        if (index <= 1) {
            return index;
        }
        Integer result = hashtable.get(index);
        if (result != null) {
            return result;
        }

        result = dynamicFibonacci(index - 1) + dynamicFibonacci(index - 2);
        hashtable.put(index, result);
        return result;
    }

    public static void main(String[] args) {
        System.out.println("value of index: 35:  " + recursiveFibonacci(35));
        System.out.println("recursive counter: " + DynamicProgrammingFibonacciMain.recursiveCounter);
        System.out.println("value of index: 35:  " + loopFibonacci(35));
        System.out.println("loop counter: " + DynamicProgrammingFibonacciMain.loopCounter);

        System.out.println("value of index: 35:  " + dynamicFibonacci(35));
        System.out.println("dynamicProgrammingCounter: " + DynamicProgrammingFibonacciMain.dynamicProgrammingCounter);
    }
}
